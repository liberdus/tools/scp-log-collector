import { resolve, join } from 'path';
import { createSnapshot } from './createSnapshot';
import { download } from './download';
import { URL } from 'url';
import { ensureExists } from './utils';
import { mapLimit } from 'async';
import * as Got from 'got';

const USERNAME = 'ec2-user';
const KEY_PATH = resolve(process.argv[2] || '~/.ssh/shardus-ent-cf.pem');
const INSTANCE_URL = process.argv[3];
const NETWORK_DIR = process.argv[4];

async function main() {
  // Ensure that the networkDir exists
  await ensureExists(NETWORK_DIR);

  // Get nodelist
  const nodelistUrl = new URL('/nodelist', `http://${INSTANCE_URL}`);
  console.log('nodelistUrl', nodelistUrl.toString());
  console.log();
  const { nodelist } = await Got.default.get(nodelistUrl.toString()).json();
  console.log('nodelist', nodelist);

  // Get tar.gz file into each instance dir
  // tslint:disable-next-line: no-any
  await mapLimit(nodelist, 5, async (node: any) => {
    const ip = node.externalIp;
    console.log(`Downloading from ${ip}...`);
    const savePath = join(NETWORK_DIR, `debug-${ip}/`);
    const snapshotTarPath = await createSnapshot(USERNAME, KEY_PATH, ip);
    await download(USERNAME, KEY_PATH, ip, snapshotTarPath, savePath);
  });
}
main();
