import { readFileSync } from 'fs';
import client = require('scp2');
import { ensureExists } from './utils';
import { resolve } from 'dns';
import { reject } from 'async';

export async function download(
  username: string,
  key: string,
  host: string,
  remotePath: string,
  localPath: string
) {
  await ensureExists(localPath);

  return new Promise((resolve, reject) => {
    client.scp(
      {
        host,
        username,
        privateKey: readFileSync(key),
        path: remotePath,
      },
      localPath,
      // tslint:disable-next-line: no-any
      (err: any) => {
        err ? reject(err) : resolve();
      }
    );
  });
}
