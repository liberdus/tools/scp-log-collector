import { mkdir } from 'fs';

// From: https://stackoverflow.com/a/21196961
export function ensureExists(dir: string) {
  return new Promise((resolve, reject) => {
    mkdir(dir, { recursive: true }, err => {
      if (err) {
        // Ignore err if folder exists
        if (err.code === 'EEXIST') resolve();
        // Something else went wrong
        else reject(err);
      } else {
        // Successfully created folder
        resolve();
      }
    });
  });
}
