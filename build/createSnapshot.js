"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const node_ssh_forward_1 = require("node-ssh-forward");
const fs_1 = require("fs");
async function createSnapshot(username, key, host) {
    const snapshotName = `snapshot-${getDatetime()}`;
    const snapshotPath = `/home/${username}/${snapshotName}`;
    const snapshotTarPath = `/home/${username}/${snapshotName}.tar.gz`;
    await ssh(username, key, host, [
        `mkdir ${snapshotPath}`,
        `docker cp $(docker ps -aqf "ancestor=registry.gitlab.com/liberdus/server"):/usr/src/app/logs ${snapshotPath}/`,
        `docker cp $(docker ps -aqf "ancestor=registry.gitlab.com/liberdus/server"):/usr/src/app/db ${snapshotPath}/`,
        `sudo cp $(docker inspect --format='{{.LogPath}}' $(docker ps -aqf "ancestor=registry.gitlab.com/liberdus/server")) ${snapshotPath}/output.log`,
        `sudo chown ${username}:${username} ${snapshotPath}/output.log`,
        `tar -czvf ${snapshotTarPath} -C ${snapshotPath} .`,
    ]);
    return snapshotTarPath;
}
exports.createSnapshot = createSnapshot;
async function ssh(username, key, host, cmds) {
    const connection = new node_ssh_forward_1.SSHConnection({
        username,
        privateKey: fs_1.readFileSync(key),
        endHost: host,
    });
    const command = cmds.join(' && ');
    try {
        await connection.executeCommand(command);
    }
    catch (err) {
        console.log(`ssh ${host}> failed to executeCommand "${command}": ${err}`);
    }
    try {
        await connection.shutdown();
    }
    catch (err) {
        console.log(`ssh ${host}> shutdown failed: ${err}`);
    }
}
function pad(num) {
    if (num < 10) {
        return '0' + num;
    }
    return num;
}
function getDatetime() {
    const date = new Date();
    return (date.getUTCFullYear() +
        '' +
        pad(date.getUTCMonth() + 1) +
        '' +
        pad(date.getUTCDate()) +
        'T' +
        pad(date.getUTCHours()) +
        ':' +
        pad(date.getUTCMinutes()) +
        ':' +
        pad(date.getUTCSeconds()) +
        '.' +
        (date.getUTCMilliseconds() / 1000).toFixed(3).slice(2, 5) +
        'Z');
}
//# sourceMappingURL=createSnapshot.js.map