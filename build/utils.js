"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
// From: https://stackoverflow.com/a/21196961
function ensureExists(dir) {
    return new Promise((resolve, reject) => {
        fs_1.mkdir(dir, { recursive: true }, err => {
            if (err) {
                // Ignore err if folder exists
                if (err.code === 'EEXIST')
                    resolve();
                // Something else went wrong
                else
                    reject(err);
            }
            else {
                // Successfully created folder
                resolve();
            }
        });
    });
}
exports.ensureExists = ensureExists;
//# sourceMappingURL=utils.js.map