"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const client = require("scp2");
const utils_1 = require("./utils");
async function download(username, key, host, remotePath, localPath) {
    await utils_1.ensureExists(localPath);
    return new Promise((resolve, reject) => {
        client.scp({
            host,
            username,
            privateKey: fs_1.readFileSync(key),
            path: remotePath,
        }, localPath, 
        // tslint:disable-next-line: no-any
        (err) => {
            err ? reject(err) : resolve();
        });
    });
}
exports.download = download;
//# sourceMappingURL=download.js.map