"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const client = require("scp2");
const USERNAME = 'ec2-user';
const KEY_PATH = '/home/aamir/.ssh/shardus-ent-cf.pem';
async function download(host, remotePath, localPath) {
    await ensureExists(localPath);
    client.scp({
        host,
        username: USERNAME,
        privateKey: fs_1.readFileSync(KEY_PATH),
        path: remotePath,
    }, localPath, 
    // tslint:disable-next-line: no-any
    (err) => {
        console.log(err);
    });
}
// From: https://stackoverflow.com/a/21196961
function ensureExists(dir) {
    return new Promise((resolve, reject) => {
        fs_1.mkdir(dir, { recursive: true }, err => {
            if (err) {
                // Ignore err if folder exists
                if (err.code === 'EEXIST')
                    resolve();
                // Something else went wrong
                else
                    reject(err);
            }
            else {
                // Successfully created folder
                resolve();
            }
        });
    });
}
//# sourceMappingURL=downloadSnapshot.js.map