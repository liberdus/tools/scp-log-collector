"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const node_ssh_forward_1 = require("node-ssh-forward");
const fs_1 = require("fs");
const USERNAME = 'ec2-user';
const KEY_PATH = '/home/aamir/.ssh/shardus-ent-cf.pem';
async function createDockerSnapshot(host) {
    const time = getDatetime();
    await ssh(host, [
        `mkdir snapshot-${time}`,
        `docker cp $(docker ps -aqf "ancestor=registry.gitlab.com/liberdus/server"):/usr/src/app/logs /home/${USERNAME}/snapshot-${time}/`,
        `docker cp $(docker ps -aqf "ancestor=registry.gitlab.com/liberdus/server"):/usr/src/app/db /home/${USERNAME}/snapshot-${time}/`,
    ]);
}
exports.createDockerSnapshot = createDockerSnapshot;
async function ssh(host, cmds) {
    const connection = new node_ssh_forward_1.SSHConnection({
        username: USERNAME,
        privateKey: fs_1.readFileSync(KEY_PATH),
        endHost: host,
    });
    const command = cmds.join(' && ');
    try {
        await connection.executeCommand(command);
    }
    catch (err) {
        console.log(`ssh ${host}> failed to executeCommand "${command}": ${err}`);
    }
    try {
        await connection.shutdown();
    }
    catch (err) {
        console.log(`ssh ${host}> shutdown failed: ${err}`);
    }
}
function pad(num) {
    if (num < 10) {
        return '0' + num;
    }
    return num;
}
function getDatetime() {
    const date = new Date();
    return (date.getUTCFullYear() +
        '' +
        pad(date.getUTCMonth() + 1) +
        '' +
        pad(date.getUTCDate()) +
        'T' +
        pad(date.getUTCHours()) +
        ':' +
        pad(date.getUTCMinutes()) +
        ':' +
        pad(date.getUTCSeconds()) +
        '.' +
        (date.getUTCMilliseconds() / 1000).toFixed(3).slice(2, 5) +
        'Z');
}
//# sourceMappingURL=run-commands.js.map