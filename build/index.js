"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = require("path");
const createSnapshot_1 = require("./createSnapshot");
const download_1 = require("./download");
const url_1 = require("url");
const utils_1 = require("./utils");
const async_1 = require("async");
const Got = require("got");
const USERNAME = 'ec2-user';
const KEY_PATH = path_1.resolve(process.argv[2] || '~/.ssh/shardus-ent-cf.pem');
const INSTANCE_URL = process.argv[3];
const NETWORK_DIR = process.argv[4];
async function main() {
    // Ensure that the networkDir exists
    await utils_1.ensureExists(NETWORK_DIR);
    // Get nodelist
    const nodelistUrl = new url_1.URL('/nodelist', `http://${INSTANCE_URL}`);
    console.log('nodelistUrl', nodelistUrl.toString());
    console.log();
    const { nodelist } = await Got.default.get(nodelistUrl.toString()).json();
    console.log('nodelist', nodelist);
    // Get tar.gz file into each instance dir
    // tslint:disable-next-line: no-any
    await async_1.mapLimit(nodelist, 5, async (node) => {
        const ip = node.externalIp;
        console.log(`Downloading from ${ip}...`);
        const savePath = path_1.join(NETWORK_DIR, `debug-${ip}/`);
        const snapshotTarPath = await createSnapshot_1.createSnapshot(USERNAME, KEY_PATH, ip);
        await download_1.download(USERNAME, KEY_PATH, ip, snapshotTarPath, savePath);
    });
}
main();
//# sourceMappingURL=index.js.map