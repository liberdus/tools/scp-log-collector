export declare function download(username: string, key: string, host: string, remotePath: string, localPath: string): Promise<unknown>;
