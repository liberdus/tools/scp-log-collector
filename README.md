# scp-log-collector

## Install

```
$ npm i -g https://gitlab.com/liberdus/tools/scp-log-collector.git
```

## Usage

```
$ scp-log-collector <ssh_key_path> <instance_url_&_port> <network_save_dir_path>
```
